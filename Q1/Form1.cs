﻿using System;
using System.Data;
using System.Data.SqlClient;

class Program
{
    static void Main()
    {
        // Replace with your connection string
        string connectionString = @"Data Source = MSI; Initial Catalog=arbolishvili; Integrated Security=True";

        using (IDbConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();

            using (IDbTransaction transaction = connection.BeginTransaction())
            using (IDbCommand command = connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = "SELECT * FROM school";

                // Create a parameter
                IDbDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "YourParameterNameHere";
                parameter.Value = "YourParameterValueHere";
                command.Parameters.Add(parameter);

                using (IDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        IDataRecord record = (IDataRecord)reader;
                        Console.WriteLine($"Column 1: {record[0]}, Column 2: {record[1]}");
                    }
                }

                transaction.Commit();
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;



namespace Quiz1
{

    class Program
    {
        static void Main()
        {
            string connectionString = @"Data Source = MSI; Initial Catalog=arbolishvili; Integrated Security=True";

            using (IDbConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                using (IDbTransaction transaction = connection.BeginTransaction())
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.Transaction = transaction;
                    command.CommandText = "SELECT * FROM school";



                    using (IDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            IDataRecord record = (IDataRecord)reader;
                            Console.WriteLine($"Column 1: {record[0]}, Column 2: {record[1]}");
                        }
                    }

                    transaction.Commit();
                }
            }
            Console.ReadLine();

        }

    }
}


using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

class Program
{
    static void Main()
    {
        string connectionString = @"Data Source = MSI; Initial Catalog=arbolishvili; Integrated Security=True";

        using (DbConnection connection = new SqlConnection(connectionString))
        {
            connection.Open();

            using (DbTransaction transaction = connection.BeginTransaction())
            using (DbCommand command = connection.CreateCommand())
            {
                command.Transaction = transaction;
                command.CommandText = "SELECT * FROM school";



                using (DbDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int columnIndex = reader.GetOrdinal("school");
                        string columnValue = reader.GetString(columnIndex);
                        Console.WriteLine(columnValue);
                    }
                }

                transaction.Commit();
            }
        }
        Console.ReadLine();

    }
}
